'use strict';
var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: {
        index: './src/index.js'
    },
    output: {
        path: path.resolve(__dirname, 'build', 'src'),
        filename: '[name].bundle.js',
        chunkFilename: '[id].bundle.js',
        publicPath: 'src/'
    },
    debug: true,
    devtool: 'sourcemap',
    module: {
        loaders: [
            { test: /\.css$/, loader: 'style/useable!css!autoprefixer?browsers=last 5 version!' },
            { test: /\.less$/, loader: 'style!css!autoprefixer?browsers=last 5 version!less!' },
            { test: /\.(eot|svg|ttf|woff|woff2)\w*/, loader: 'file' },
            { test: /\.html$/, loader: 'raw' },
            { test: /\.(png|jpeg|jpg)/, loader:'url-loader'},
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /(node_modules|bower_components)/,
                query: {
                    presets: [
                        'es2015'
                    ]
                }
            },
        ]
    },
    resolve: {
        root: [
            path.resolve(__dirname),
            path.resolve(__dirname, 'src/'),
            path.resolve(__dirname, 'src/framework/')
        ]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin('common.bundle.js')
    ]
};
