/**
 *  SSOConfig set authorised configuration.
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
import ConfiguratorBase from 'lib/ConfiguratorBase';

export default class Configurator extends ConfiguratorBase {
    constructor(features, app) {
        super(features, app);
    }

    httpConfig($httpProvider) {
        $httpProvider.defaults.headers.common.Accept = 'application/json;charset=utf-8';
        $httpProvider.defaults.withCredentials = true;
    }

    execute() {
        this.httpConfig.$inject = ['$httpProvider'];
        this.config(this.httpConfig);
    }
}
