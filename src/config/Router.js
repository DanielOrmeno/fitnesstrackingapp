/**
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
import ConfiguratorBase from 'lib/ConfiguratorBase';

export default class Configurator extends ConfiguratorBase {
    constructor(features, app) {
        super(features, app);
    }
    
    routeConfig($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/login');
        $stateProvider.state('login', {
            url: '/login',
            template: require('../components/login/views/login.html'),
            controller: 'LoginController',
            controllerAs: 'login'
            }).state('home', {
            url: '/home',
            template: require('../components/shell/views/shell.html'),
            controller: 'ShellController',
            controllerAs: 'home'
            }).state('home.dashboard', {
            url: '/dashboard',
            template: require('../components/dashboard/views/dashboard.html'),
            controller: 'DashboardController',
            controllerAs: 'home'
            }).state('home.strap', {
            url: '/strap',
            template: require('../components/straptester/views/strap.html'),
            controller: 'StrapController',
            controllerAs: 'home'
            });
    }
    
    execute() {
        var routeConfig = this.routeConfig.bind(this);

        routeConfig.$inject = [
            '$stateProvider',
            '$urlRouterProvider'
        ];
        this.config(routeConfig);
    }
}
        
        