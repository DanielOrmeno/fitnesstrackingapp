/**
 *  NotifierConfig set angular-sweetnotifier needed configuration
 *
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
import ConfiguratorBase from 'lib/ConfiguratorBase';

export default class Configurator extends ConfiguratorBase {
    constructor(features, app) {
        super(features, app);
    }

    notifierConfig(notifierProvider) {
        notifierProvider.setPlacement('top', 'right');
        notifierProvider.setUseNativeWhileBlur(true);
    }

    execute() {
        this.notifierConfig.$inject = ['notifierProvider'];
        this.config(this.notifierConfig);
    }
}
