/**
 *  Entrance of config
 *
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
import app from './AppConfig';
import notifier from './NotifierConfig';
import router from './Router';
import sso from './SSOConfig';

export default [
    app,
    notifier,
    router,
    sso
];
