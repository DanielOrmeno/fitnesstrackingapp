/**
 *  Entrance of services
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
import events from './Events';
import utils from './Utils';

export default [events, utils];
