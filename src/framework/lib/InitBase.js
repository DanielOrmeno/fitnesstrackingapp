/**
 *  InitBase class
 *
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
class InitBase {

    constructor(features) {
        this.features = features;
    }

    execute() {
    }
}

export default InitBase;
