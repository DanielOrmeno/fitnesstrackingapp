/**
 *  BodyInit set ng-view to the index.html.
 *
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
import InitBase from 'lib/InitBase';
import { element } from 'angular';

class Initializer extends InitBase {

    constructor(features) {
        super(features);
    }

    execute() {
        element(document.body).append('<div ui-view autoscroll="true" id="ESWrapper" class="main"></div>');
    }
}

export default Initializer;
