/**
 *  Defines the ShellController controller
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
*/
'use strict';
class ShellController{
    constructor ($scope, events, ShellService, snapRemote){
        var self = this;
        this.scope = $scope;
        this.drawer;
        this.isDrawerOpen = false;
        this.notificationProvider = (type, msg) => events.emit('alert', {type: type, message: msg});
        
        $scope.menuItems = [
            {
                DisplayName: 'Home',
                Icon: 'fa-tachometer',
                Route: 'home.dashboard'
            },
            {
                DisplayName: 'Log Progress',
                Icon: 'fa-check-square-o',
                Route: 'home.logProgress'
            },
            {
                DisplayName: 'My Statistics',
                Icon: 'fa-line-chart',
                Route: 'home.strap'
            },
            {
                DisplayName: 'Settings',
                Icon: 'fa-cog',
                Route: 'home.settings'
            }
        ];
        
        //- Nav Drawer
        this.initializeNavigationDrawer(snapRemote);
        $scope.openDrawer  = () => self.openNavigationDrawer();
        $scope.closeDrawer  = () => self.closeNavigationDrawer();
        $scope.select= (item) => $scope.selected = item;
        $scope.isActive = (item) => $scope.selected === item;
        //- Destructor
        $scope.$on('$destroy', function() {});
    }
    
    //- Controller Methods
    initializeNavigationDrawer(SnapRemote){
        var self = this;
        if(typeof SnapRemote === 'undefined'){
            console.log('An instance of the snapRemote service, from the snap-angular module was not found, please check your dependencies');
            return;
        }
        
        SnapRemote.getSnapper().then(function(snapper) {
            var myself = self;
            self.drawer = snapper;
            //- events configured here as promise can be async
            self.drawer.on('end', ()=>{
                myself.isDrawerOpen = myself.drawer.state().state === 'left';     
            });
        });
        
        this.scope.ESNavDrawerOptions = {
            disable: 'right',
            maxPosition: 150
        };
        
        setTimeout(() =>{self.openNavigationDrawer();}, 250);
        this.scope.selected = this.scope.menuItems[0];
    }
    
    openNavigationDrawer(){
        this.drawer.open('left');
    }
    
    closeNavigationDrawer(){
        if (this.isDrawerOpen)
            this.drawer.close();
    }
}

ShellController.$inject = [
    '$scope',
    'events',
    'ShellService',
    'snapRemote'
];

export default ShellController;