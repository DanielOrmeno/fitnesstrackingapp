/**
 *  Defines the ShellService
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
var ShellService = function($http, utils) {

};

ShellService.$inject = ['$http', 'utils'];

export default ShellService;
