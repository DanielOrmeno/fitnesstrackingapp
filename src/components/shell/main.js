/**
 * ******************************************************************************************************
 *
 *   Defines a home feature
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 * ******************************************************************************************************
 */
'use strict';
import ShellController from './controllers/ShellController';
import ShellService from './service/ShellService';
import customTpl from './views/custom.html';
import FeatureBase from 'lib/FeatureBase';
import navbar from './views/navbar.html';
import aside from './views/aside.html';
import {element} from 'angular';

export default class Feature extends FeatureBase {

    constructor() {
        super('home');
        this.$body = element(document.body);
    }
    
    beforeStart() {
        this.$body.prepend(navbar);
    }

    templateCaching($templateCache) {
        $templateCache.put('aside', aside);
    }
    
    HeaderCtrl($scope) {
    }

    execute() {
        this.templateCaching.$inject = ['$templateCache','navbar'];
        this.HeaderCtrl.$inject = ['$scope'];
        this.controller('HeaderCtrl', this.HeaderCtrl);
        
        //- Snap Wrapper
        this.controller('ShellController', ShellController);
        this.service('ShellService', ShellService);
        this.run([
            '$templateCache',
            function($templateCache) {
                $templateCache.put('customTpl', customTpl);
            }
        ]);
    }
}
