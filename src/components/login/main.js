/**
 * ******************************************************************************************************
 *
 *   Defines a about feature
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 * ******************************************************************************************************
 */
'use strict';
import FeatureBase from 'lib/FeatureBase';
import LoginController from './controller/LoginController';
import LoginService from './service/LoginService';

class Feature extends FeatureBase {

    constructor() {
        super('login');
    }

    execute() {
        this.controller('LoginController', LoginController);
        this.service('LoginService', LoginService);
    }
}

export default Feature;
