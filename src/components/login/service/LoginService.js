/**
 *  Defines the AboutService
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
var LoginService = function($http, utils) {
	
	this.getUsers = function() {
        return $http.get(utils.getApi('/users'));
    };
};

LoginService.$inject = ['$http', 'utils'];

export default LoginService;
