/**
 *  Defines the AboutController controller
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
class LoginController {
    constructor($scope, LoginService, events, $state ) {
        var self = this;
        this.notificationProvider = (type, msg) => events.emit('alert', {type: type, message: msg});
        this.loginService = LoginService;
        this.scope = $scope;
        this.state = $state;
        
        $scope.ESUser = {
            email: '',
            password: '',
        };
        
        //- Bindings
        $scope.attemptLogin  = () => self.loginUser();
        $scope.$on('$destroy', function() {});
    }
    
    loginUser(){
        var self = this;
        this.loginService.getUsers().success(data => {
            var myself = self;
            
            for (var i = 0; i < data.length; i++){
                if(data[i].email === myself.scope.ESUser.email && data[i].password === myself.scope.ESUser.password ){
                    myself.notificationProvider('success', 'Welcome '+data[i].name);
                    //- Funky dodgy animations
                    myself.scope.animation = 'animated slideOutDown';
                    setTimeout(()=>{
                        myself.state.go('home.dashboard');
                    }, 500);
                    return;
                }
            }
            myself.notificationProvider('error', 'Invalid credentials. Hint: [yourName]@me.com, 12345');
        });
    }
};

LoginController.$inject = [
    '$scope',
    'LoginService',
    'events',
    '$state'
];

export default LoginController;
