/**
 *  Defines the AboutService
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
var StrapService = function($http, utils) {

    this.getStates = function() {
        return $http.get(utils.getApi('/states'));
    };

    this.getMenus = function() {
        return $http.get(utils.getApi('/menus'));
    };

    this.getDropdown = function() {
        return $http.get(utils.getApi('/dropdown'));
    };

};

StrapService.$inject = ['$http', 'utils'];

export default StrapService;