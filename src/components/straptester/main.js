/**
 * ******************************************************************************************************
 *
 *   Defines a about feature
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 * ******************************************************************************************************
 */
'use strict';
import StrapController from './controller/StrapController';
import StrapService from './service/StrapService';
import customTpl from '../shell/views/custom.html';
import FeatureBase from 'lib/FeatureBase';
import aside from '../shell/views/aside.html';
import {element} from 'angular';

export default class Feature extends FeatureBase {

    constructor() {
        super('strap');
    }
    
    beforeStart() {
    }

    templateCaching($templateCache) {
        $templateCache.put('aside', aside);
    }

    execute() {
        this.templateCaching.$inject = ['$templateCache'];
        
        //- Snap Wrapper
        this.controller('StrapController', StrapController);
        this.service('StrapService', StrapService);
        this.run([
            '$templateCache',
            function($templateCache) {
                $templateCache.put('customTpl', customTpl);
            }
        ]);
    }
}
