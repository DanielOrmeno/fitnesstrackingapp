/**
 *  Entrance of common ui
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';

import alerts from './modules/Alerts';
import autofocus from './modules/Autofocus';
import confirm from './modules/Confirm';
import error from './modules/Error';
import footer from './modules/Footer';
import info from './modules/Info';
import stRatio from './modules/StRatio';

export default [
    alerts,
    autofocus,
    confirm,
    error,
    footer,
    info,
    stRatio
];