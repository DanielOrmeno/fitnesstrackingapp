/**
 *  Defines the Alerts
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';

import FeatureBase from '../../../framework/lib/FeatureBase';
import { element } from 'angular';

var TYPES = {
    warning: 'warning',
    error: 'error',
    info: 'info',
    success: 'success'
};

var TIMEOUTS = {
    warning: 3000,
    error: 4000,
    info: 2000,
    success: 2000
};

var TITLES = {
    warning: 'Warning',
    error: 'Error',
    info: 'Info',
    success: 'Success'
};

class Feature extends FeatureBase {
    constructor() {
        super('Alerts');
        this.$body = element(document.body);
    }

    beforeStart() {
        this.$body.append('<sweetnotifier></sweetnotifier>');
    };

    alertEvent(events, notifier) {
        events.on('alert', function(data) {
            notifier.emit({
                type: TYPES[data.type],
                title: TITLES[data.type],
                content: data.message,
                timeout: TIMEOUTS[data.type]
            });
        });
    }

    execute() {
        this.alertEvent.$inject = ['events', 'notifier'];
        this.run(this.alertEvent);
    }
}

export default Feature;
