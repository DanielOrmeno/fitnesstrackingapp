/**
 *  Defines the AboutService
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
var DashboardService = function($http, utils) {

    this.getDemoList = function() {
        return $http.get(utils.getApi('/demolist'));
    };

};

DashboardService.$inject = ['$http', 'utils'];

export default DashboardService;
