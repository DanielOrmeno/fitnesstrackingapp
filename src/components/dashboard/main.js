/**
 * ******************************************************************************************************
 *
 *   Defines a about feature
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 * ******************************************************************************************************
 */
'use strict';
import FeatureBase from 'lib/FeatureBase';
import DashboardController from './controller/DashboardController';
import DashboardService from './service/DashboardService';

class Feature extends FeatureBase {

    constructor() {
        super('dashboard');
    }

    execute() {
        this.controller('DashboardController', DashboardController);
        this.service('DashboardService', DashboardService);
    }
}

export default Feature;
