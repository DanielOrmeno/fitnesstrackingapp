/**
 *  Defines the AboutController controller
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
class DashboardController{
    constructor ($scope, DashboardService, events) {
    $scope.showSpinner = true;
    
    $scope.$on('$destroy', function() {});
    }
};

DashboardController.$inject = [
    '$scope',
    'DashboardService',
    'events'
];

export default DashboardController;
