/**
 *  Entrance of features
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
import common from './common/main';
import shell from './shell/main';
import dashboard from './dashboard/main';
import login from './login/main'
import strap from './straptester/main'
export default [shell, ...common, login, dashboard, strap];
