/**
 *  index.js launch the application.
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
require.ensure(['splash-screen/splash.min.css', 'splash-screen'], function(require) {

    require('splash-screen/splash.min.css').use();
    require('splash-screen').enable('circular');
});

require.ensure(['styles/main.less', 'splash-screen', './main'], function(require) {

    require('styles/main.less');
    require("font-awesome-webpack");

    var App = require('./main').default;
    (new App()).run();
});

