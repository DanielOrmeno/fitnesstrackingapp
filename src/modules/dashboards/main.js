'use strict';
//- Templates
import new_widget_modal from './templates/new-widget-modal.html';
import template_dashboard from './templates/dashboard-container.html';
//- Templates widgets
import widget_ranking from './templates/widget-ranking.html';
import widget_myweight from './templates/widget-myweight.html';
import widget_myprogress from './templates/widget-myprogress.html';
//- Controllers
import DashboardController from './js/controllers/dashboard_controller.js';
import ModalController from './js/controllers/modal-controller.js';
//- Controllers widgets
import MyWeightController from './js/controllers/myweight-controller.js';

angular.module('ESDashboards', ['ui.sortable', 'ui.bootstrap']).
config(function(){
	return true; // Module Configuration if any
})

.controller('EsDashboardController', ['$scope', '$uibModal', function($scope, $uibModal) {
   return new DashboardController($scope, $uibModal);
}])

.controller('ModalController', function($scope, $uibModalInstance, types) {
    return new ModalController($scope, $uibModalInstance, types);
})

.controller('MyWeightController', function($scope){
    return new MyWeightController($scope);
})

.directive('esDashboard', function(){
    return {
        restrict: 'AE',
        templateUrl: 'dashboard-container'
    };
})

.directive('esDashboardWidget', ['$templateCache', function($templateCache){
    return {
        transclude:true,
        templateUrl: function(elem, attr){
            return 'widget-'+attr.widget;
        }
    }
}])

.run(["$templateCache", function($templateCache) {
    $templateCache.put("widget-ranking", widget_ranking);
    $templateCache.put("widget-myweight", widget_myweight);
    $templateCache.put("widget-myprogress", widget_myprogress);
    $templateCache.put("dashboard-container", template_dashboard);
    $templateCache.put("new-widget-modal", new_widget_modal);
}]);