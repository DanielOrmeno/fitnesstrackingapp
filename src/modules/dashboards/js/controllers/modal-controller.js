export default class ModalController{
    constructor($scope, $uibModalInstance, items){
        $scope.widgetTypes = items;
        
        $scope.ok = function () {
            var selection = $scope.selected;
            $uibModalInstance.close(selection.id);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.select= (item) => $scope.selected = item;
    }
}