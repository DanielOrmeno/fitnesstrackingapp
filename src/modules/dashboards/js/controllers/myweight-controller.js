export default class MyWeightController{
    constructor($scope){
        this.scope = $scope;
        this.LastWeight = '0'
        this.LastWeightDate = this.formatDate();
        this.System = 'Kg'; //TODO - Use Enum
        this.bindings();
    }
    
    bindings(){
        this.scope.LastWeight = this.LastWeight;
        this.scope.LastWeightDate = this.LastWeightDate;
        this.scope.System = this.System;
    }
    
    formatDate(){
        //- TODO - add support for different formats
        var date = new Date();
        return date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear();
    }
}