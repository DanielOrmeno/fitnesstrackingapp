'using strict';
import WidgetRanking from '../classes/widget-ranking.js';
import WidgetMyWeight from '../classes/widget-myweight.js';
import WidgetMyProgress from '../classes/widget-myprogress.js';
import ModalOptions from '../classes/modal-options.js';

export default class DashboardController {
    constructor($scope, $uibModal){
        this.scope = $scope;
        this.modalService = $uibModal;
        
        //-todo: move to better place
        this.widgetTypes = [{name: 'Orders', id: 1}, {name: 'My Weight', id: 2}, {name: 'Total Sales', id: 3}];
        this.scope.widgetSizes = ['small', 'medium', 'large'];
        
        this.scope.dashboardWidgets = [
            new WidgetMyProgress(), new WidgetRanking(), new WidgetMyWeight()
        ];
        
        this.scope.SortableOptions = {
            update: function(e, ui) { return }
        };
        
        //- Initialize Controller
        this.configureModals();
        this.bindings();
    }
    
    bindings(){
        var self = this;
        
        this.scope.addWidget = ()=> {
            self.addWidget();
        };
        
        this.scope.removeWidget = (widget) => {
            self.removeWidget(widget);
        }
        
        this.scope.toggleFlip = (widget) => {
            self.flipWidget(widget);
        }
    }
    
    configureModals(){
        var self = this;
        this.scope.animationsEnabled = true;
    }
    
    //- Binding Methods
    addWidget(){
        var self = this;
        var template = 'new-widget-modal';
        var resolve = { types: function (){ return self.widgetTypes }  };
        var modalInstance = self.modalService.open(new ModalOptions(true, template, 'md', resolve));
        modalInstance.result.then((selectedItem) => {
            var widget = selectedItem === 1 ? new WidgetRanking() : selectedItem === 2 ? new WidgetMyWeight() : new WidgetMyProgress();
            self.scope.dashboardWidgets.push(widget);
        });
    }
    
    removeWidget(widget){
        this.scope.dashboardWidgets.splice(widget, 1);
    }
    
    flipWidget(widget){
        widget.flip();
    }
}