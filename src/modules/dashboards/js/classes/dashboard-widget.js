'use strict';
export default class DashboardWidget{
    constructor(type, title, size){
        this.widgetType = type;
        this.title = title;
        this.size = this.resolveSize(size);
        this.showSettings = false;
        this.displaySize;
    }
    
    resolveSize(size){
        var baseSize = 'col-sm-12';
        
        switch(size){
            case 'sm':
            case 'small':
            case 's':
                this.displaySize = 'small';
                return baseSize+' col-md-4';
                break;
            case 'md':
            case 'medium':
            case 'm':
                this.displaySize = 'medium';
                return baseSize+' col-md-8';
                break;
            case 'lg':
            case 'large':
            case 'l':
                this.displaySize = 'large';
                return baseSize+' col-md-12';
                break;
        }
        this.displaySize = 'small';
        return baseSize+' col-md-4';
    }
    
    flip() { this.showSettings = !this.showSettings;}
    setSize(s){ this.size = this.resolveSize(s);}
    sizeClass(){
        var size = this.displaySize === 'small' ? 'sm' : this.displaySize === 'medium' ? 'md' : 'lg';
        return this.resolveSize(size);
    }
}