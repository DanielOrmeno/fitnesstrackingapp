export default class ModalOptions {
    
    constructor(animate, template, size , resolve){
      this.animation = animate;
      this.templateUrl = template;
      this.controller = 'ModalController';
      this.size = size;
      this.resolve = resolve;
    }
}