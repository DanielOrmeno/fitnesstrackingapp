'use strict';
import DashboardWidget from './dashboard-widget.js';

export default class WidgetMyProgress extends DashboardWidget {
    constructor(){
        super('myprogress','My Progress', 'lg');
        this.isMyProgress = true;
    }
}