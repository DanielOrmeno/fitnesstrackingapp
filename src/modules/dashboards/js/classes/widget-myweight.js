'use strict';
import DashboardWidget from './dashboard-widget.js';

export default class WidgetMyweight extends DashboardWidget {
    constructor(){
        super('myweight','My Weight', 'sm');
        this.isMyWeight = true;
    }
}