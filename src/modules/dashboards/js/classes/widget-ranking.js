'use strict';
import DashboardWidget from './dashboard-widget.js';

export default class WidgetRanking extends DashboardWidget {
    constructor(){
        super('ranking','Ranking', 'sm');
        this.isRanking = true;
    }
}