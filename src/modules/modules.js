/**
 *  Entrance of features
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
import 'angular-animate';
import 'angular-ui-router';
import 'angular-sanitize';
import 'angular-sweetnotifier';
import 'angular-theme-spinner';
import 'angular-smart-table';
import 'angular-ui-sortable';
import 'angular-ui-bootstrap';
import 'bower_components/angular-snap/angular-snap';
import 'bower_components/snapjs/snap';
//- Custom Modules
import './dashboards/main.js'

export default [
    'ngAnimate',
    'ui.router',
    'ngSanitize',
    'angular-sweetnotifier',
    'angular-theme-spinner',
    'smart-table',
    'ui.sortable',
    'ui.bootstrap',
    'snap',
    'ESDashboards'
];