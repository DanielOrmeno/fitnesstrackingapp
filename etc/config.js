/**
 *
 *  config.js which contains the configuration of app, and it should never be cached
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 **/
'use strict';

export default {
    'appname': 'ESWebApp',
    'base': '',
    'version': '1.0.0',
    'icp': 'ICP here',
    'protocol': 'https://raw.githubusercontent.com/leftstick/generator-es6-angular/master/LICENSE',
    'api': '/mock'
};
