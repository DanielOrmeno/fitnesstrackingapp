/**
 *  test suite entrance
 *  All the unit-test cases should be imported here
 *
 *  @author  Daniel Ormeno
 *  @date    Dec 10, 2015
 *
 */
'use strict';
import 'angular';
import 'angular-mocks';

//unit-test cases
import './features/home/controller/HomeControllerTest';
import './features/home/service/HomeServiceTest';
