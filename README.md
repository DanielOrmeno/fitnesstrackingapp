# Fitness Tracker WebApp
## Overview

## Build / Setting up your environment
Clone this repository and navigate to the root of its local directory


Install all dependencies by running ` npm install ` and ` bower install `

Run ` gulp dev ` for running in dev mode. `gulp release` and `gulp test` are also available

The website will be displayed in http://localhost:8080/webpack-dev-server/index.html

### Troubleshooting Build.
If you cant run gulp, or it fails due to missing dependencies, try manually installing the dependencies

----

If running from a Macbook, ensure requirements for node-gyp are set (Xcode Tools)
https://github.com/nodejs/node-gyp

If the command line tools for Xcode are required, run the following command from terminal

`
xcode-select --install
`

Further dependency resolution might be needed for Windows.
Please document findings.

## Dependencies
### Dev-Dependencies / Tools
* App file structure / Automation using [yeoman generator](https://github.com/leftstick/generator-es6-angular)
* [Webpack](https://webpack.github.io/)
* [Gulp](http://gulpjs.com/)

### Prod-Dependencies
* Angujar JS
* Bootstrap
* Less 
* Font Awesome
* Lowdash
* Animate CSS

## Testing

Running 
`gulp test`
will run the unit tests with karma.